package exams.first;

import java.util.Scanner;

public class Repetition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		System.out.print("Enter an integer: ");
	        
	    double dim = input.nextDouble();
	        
	    int b, h;
	    
        for(b=0; b<dim; b++)
        {
 
            for(h=0; h<=b; h++)
            {
             
                System.out.print("* ");
            }
 
                System.out.println();
        }
	
	
	}

}
