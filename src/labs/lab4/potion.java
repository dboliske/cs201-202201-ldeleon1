package labs.lab4;

public class potion {

	private String name; 
	private double strength;
	
	public potion() {
		String name = "Elixir";
		double stength = 37;
		
	}
	
	public potion(String name, double strength) {
		
		this.name = "Elixir";
		
		setName(name);
		 
		this.strength = 43.12561;
		
		setStrength(strength);
	 
	}
	 
	 public void setName(String name) {	//mutator
		 
		 if (name.length() >=0) {
		
			 this.name = name;
		 
		 }
	 }
	 
	 public void setStrength(double stength) {	//mutator
		 
		 if (strength >= 0) {
				
			 this.strength = strength;
	 }
 }
	
	 public String getName() {	//accessor
			
		 return name;
		}

	 public double getStrength() {	//accessor
		 	
		 return strength;
	 }

	 public String toString() {	//string method format
		 
		 return "Potion:" + name + " " +"Strength: "+ strength;
	 }
	 	
	 public boolean validStrength(double strength) {	//checks to see if returns "true"
			
		 if (strength >= 0 && strength <= 34) {
				
			 return true;
			 
		 	}
		
		 else { 
				return false;
		 }
	
	 }
	 
	
}


