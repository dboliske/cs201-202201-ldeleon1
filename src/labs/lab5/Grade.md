# Lab 5

## Total

14/20

## Break Down

CTAStation

- Variables:                    2/2
- Constructors:                 0.5/1
- Accessors:                    0/2
- Mutators:                     2/2
- toString:                     0.5/1
- equals:                       0/2

CTAStopApp

- Reads in data:                1/2
- Loops to display menu:        2/2
- Displays station names:       1/1
- Displays stations by access:  2/2
- Displays nearest station:     2/2
- Exits                         1/1

## Comments
- Did not implement inheritance
- Class methods lack implemention
- CTAStopApp menu options aren't working for user input