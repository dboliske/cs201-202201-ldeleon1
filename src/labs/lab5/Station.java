package labs.lab5;

public class Station {

	private String stationName;
	
	private double lat;
	
	private double lng;
	
	private String description;
	
	public boolean wheelchair(Object obj) {
			
			if (!(obj instanceof Station)) {
				
				return false;	
			}
	
	 Station s = (Station) obj; 
		
		if (!super.equals(s)) {
		
			return false;
			
		} else return true;}

	 
	private int[] stopNumbers;

	
	private boolean wheelchair;
	
	
	public Station() {
	
		setStationName("wa");
		setLat(0);
		setLng(0);
		setDESCRIPTION("elevated");
		setWHEELchair(true);
		setSTOPnumber(new int[] {-1, -1, -1, -1, -1, -1, -1});
	}
	

	
	public Station(String stationName, double lat, double lng, String description, boolean wheelchair, int[] stopNumbers) {
		
		
		setStationName(stationName);
		setLat(lat);
		setLng(lng);
		setDESCRIPTION(description);
		setWHEELchair(wheelchair);
		setSTOPnumber(stopNumbers);
		
	}

	public void setStationName(String stationName) {	
		
			 this.stationName = stationName;
		 	
		}

	public String getStationName() {	
		 return stationName;
		}
		
	public void setLat(double lat) {	
		
		if (lat >= 0) {
			
			this.lat = lat;
		}
	}

	 public double getLat() {	
		 return lat;
	}

	 public void setLng(double lng) {	
			
		 if (lng >= -100) {
			 this.lng = lng;
		 }
	}
	 
	 public double getLng() {	
		 return lng;
	}
	 
	 
	 public String getDESCRIPTION() {	
		 return description;
	}
	 
	 public void setDESCRIPTION(String description) {	
		 this.description = description;
	}
		
	 public boolean getWHEELchair() {	
		 return wheelchair;
	}
	 
	 public void setWHEELchair(boolean wheelchair) {	
		 this.wheelchair = wheelchair;
	}
	 
	 public int[] getSTOPnumber() {	
		 return stopNumbers;
	}
	 
	 public void setSTOPnumber(int[] stopNumbers) {	
		 this.stopNumbers = stopNumbers;
	}
	 
	 
	 public boolean equals(Object obj) {
		 return obj instanceof Station && 
			
		stationName.equals( ((Station) obj) .stationName) &&
				 
		description.equals( ((Station) obj) .description) &&	
		
		lat ==  ((Station) obj) .lat &&
		
		lng ==  ((Station) obj) .lng &&
		
		wheelchair ==  ((Station) obj) .wheelchair; 
	}
		
	@Override
	 public String toString(){	
		 
		String body = "Name: " + stationName + "\n" + 
				"Lat: " + lat + "\n" +
				"Lng: " + lng + "\n" +
				"Description: " + description + "\n" +
				"Wheelchair: " + wheelchair + "\n";
		
		for (int pos : stopNumbers ) {
			
			body += pos + ",";
		}
		
		return body;
		
			
	}
}