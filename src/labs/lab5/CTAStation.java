package labs.lab5;

public class CTAStation {

	private String name;
	
	private String location;
	
	private boolean wheelchair;
	
	private boolean open;
	
	public CTAStation() {}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {}
	
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void SetName(String name) {}
	
	public void SetLocation(String location) {}
	
	public void SetWheelchair(boolean wheelchair) {}
	
	public void SetOpen(boolean open) {}
	
	 public String toString() {
		return location;
		 
	 }

	 public boolean equals() {
		return open;}
}
