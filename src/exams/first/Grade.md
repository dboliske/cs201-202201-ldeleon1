# Midterm Exam

## Total

83/100

## Break Down

1. Data Types:                  18/20
    - Compiles:                 5/5
    - Input:                    3/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   13/20
    - Compiles:                 5/5
    - Selection Structure:      5/10
    - Results:                  3/5
3. Repetition:                  18/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               3/5
4. Arrays:                      13/20
    - Compiles:                 5/5
    - Array:                    3/5
    - Exit:                     5/5
    - Results:                  0/5
5. Objects:                     17/20
    - Variables:                5/5
    - Constructors:             5/5
    - Accessors and Mutators:   5/5
    - toString and equals:      2/5

## Comments

1. Alright, but the input should be read in as an int, not a double and it needs to be validated using try/catch.
2. Needs to validate input again and all of the conditions are wrong, using divide rather than modulus.
3. Needs to validate input.
4. Program does not read in words or count the number of times they occur.
5. Good, but the `equals` method does not correctly compare instances.
