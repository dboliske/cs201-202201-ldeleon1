package exams.first;

public class Pet {
	private String name;
	private int age;
	
	public Pet() {
		String name = "Dog";
		int age = 3;
		
	}
	
	public Pet(String name, int age) {
		
		this.name = "Dog";
		
		setName(name);
		 
		this.age = 3;
		
		setAge(age);
	 
	}
	 
	 public void setName(String name) {	//mutator
		 
		 if (name.length() >=0) {
		
			 this.name = name;
		 
		 }
	 }
	 
	 public void setAge(int age) {	//mutator
		 
		 if (age >= 0) {
				
			 this.age = age;
	 }
 }
	
	 public String getName() {	//accessor
			
		 return name;
		}

	 public double getAge() {	//accessor
		 	
		 return age;
	 }

	 public String toString() {	//string method format
		 
		 return "Pet:" + name + " " +"Age: "+ age;
	 }
	 	
	 public boolean equals(Object obj) {	//checks to see if returns "true"
			
		 if (age >= 0) {
				
			 return true;
			 
		 	}
		
		 else { 
				return false;
		 }
	}

}