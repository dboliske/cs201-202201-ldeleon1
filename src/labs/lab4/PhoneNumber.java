package labs.lab4;

public class PhoneNumber {

		private String countryCode;	//instance variable

		private String areaCode;	//instance variable
		
		private String number;	//instance variable

		public PhoneNumber() {	//default constructor
			
			countryCode = "+1";
			
			areaCode = "773";
			
			number = "2025862";
		}

		public PhoneNumber(String countryCode, String areaCode, String number) {	//non default constructor

		this.countryCode = "+1";
		
		setCountryCode(countryCode);
		
		this.areaCode = "773";
		
		setAreaCode(areaCode);
		
		this.number = "2025862";
		
		setNumber(number);
			
		}

		 public void setCountryCode(String countryCode) {	//mutator
				
			 if (countryCode.length() >= 1 && countryCode.length() < 15) {
					
				 this.countryCode = countryCode;
				
			 	}
			}
		 
		 public void setAreaCode(String areaCode) {	//mutator
			 
			 if (areaCode.length() == 3) {
			
				 this.areaCode = areaCode;
			 
			 }
		 }
		 
		 public void setNumber(String number) {	//mutator
			 
			 if (number.length() == 7) {
			
				 this.number = number;
			 
			 }
		 }
	
		 public String getCountryCode() {	//accessor
				
			 return countryCode;
			}

		 public String getAreaCode() {	//accessor
			 	
			 return areaCode;
		 }

		 public String getNumber() {	//accessor
			 	
			 return number;
		 }
		 
		 public String toString() {	//string method format
			 
			 return (countryCode + areaCode + number);
		 }
		 	
		 public boolean validAreaCode(double areaCode) {	//checks to see if returns "true"
				
			 if (areaCode == 3) {
					
				 return true;
				
			 	}
			
			 else { 
					return false;
			 }
		 
		 }
		 
		 public boolean validNumber(double number) {	//checks to see if returns "true"
				
			 if (number == 7) {
					
				 return true;
				 
			 	}
			
			 else { 
					return false;
			 }
		 
		 }
		
}
