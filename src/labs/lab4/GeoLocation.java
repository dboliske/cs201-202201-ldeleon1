package labs.lab4;

public class GeoLocation {   

		 private double lat;	//instance variable
		
		 private double lng;	//instance variable
	
		 public GeoLocation() {	//default constructor
		  
		lat = 29.69508;
		
		lng = 43.12561;
		
		 }
		
		 public GeoLocation(double lat, double lng) {	//non default constructor
		 
			this.lat = 29.69508;
			
			setLat(lat);
			 
			this.lng = 43.12561;
			
			setLng(lng);
		 
		 }
		 
		 public void setLat(double lat) {	//mutator
				
			 if (lat >= -90 && lat <= 90) {
					
				 this.lat = lat;
				
			 	}
			}
		 
		 public void setLng(double lng) {	//mutator
			 
			 if (lng >= -180 && lng <= 180) {
			
				 this.lng = lng;
			 
			 }
		 }
		 
	
		 public double getLat() {	//accessor
				
			 return lat;
			}

		 public double getLng() {	//accessor
			 	
			 return lng;
		 }

		 public String toString() {	//string method format
			 
			 return "(" + lat + "," + lng + ")";
		 }
		 	
		 public boolean validLat(double lat) {	//checks to see if returns "true"
				
			 if (lat >= -90 && lat <= 90) {
					
				 return true;
				 
			 	}
			
			 else { 
					return false;
			 }
		
		 }
		 
		 public boolean validLng(double lng) {	//checks to see if returns "true"
				
			 if (lng >= -180 && lng <= 180) {
					
				 return true;
				 
			 	}
			
			 else { 
					return false;
			 }
		 
		 }
		 
		


		
}
		 