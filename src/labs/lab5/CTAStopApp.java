package src.labs.lab5;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class CTAStopApp {
	
	static List<Station> stations = new ArrayList<Station>();

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Main menu
		
		Scanner scanner = new Scanner(System.in).useDelimiter("\n");
		
		
		
//		System.out.println(stations);
	
		int choice = 0;
		
		do { 
			
			String input = userIn(scanner, "1. Stations \n" + 
										   "2. Wheelchair \n" +
										   "3. Find nearest station \n" +
										   "4. Exit \n");
			
			switch (input) {
				case "1":
					choice = 1;
					displayStationNames(scanner);
					break;
					
				case "2":
					choice = 5;
					displayByWheelchair(scanner);
					break;
					
				case "3":
					choice = 6;
					displayNearest(scanner);
					break;
					
				case "4":
					choice = 8;
					print("You have exited.");
					break;

				default:
					
					System.out.print("You didn't enter a number between 1 and 4: \n");
			
			}
			
		} while (choice < 4);
		
		
	}

	static String userIn(Scanner scanner, String msg) {
		
		print (msg + " ");
	
		return scanner.next();
	}
	
	
	
	static	void print(String msg) {System.out.println(msg);}
	
	static void printStations(List<Station> printingStations) {
		
		for (int i = 0; i < printingStations.size(); i++) {
			
			System.out.println((i+ 1) + "\n\n" + printingStations.get(i));
		}
	}
		
	static List<String> fileReader(File file) {
		
		List<String> stringsFile = new ArrayList<String> ();
		
		Scanner scanner = null;
		
		try {
		
		scanner = new Scanner(file).useDelimiter("\n");
	
		} catch (Exception e ) {
		
			e.printStackTrace();
			
		}
		
		scanner.next();
		
		while(scanner.hasNext()) {
			
			stringsFile.add(scanner.next());
		}
			
		scanner.close();
		
		return stringsFile;
	
	}	
	
	
	static String userNumInt(Scanner scanner, String msg) {
		
		boolean isNum = false;
		
		while (isNum == false) {
			
			print (msg + " ");
		
			String userInput = scanner.next();	
			
			try {
				
				Integer.parseInt(userInput);
				
				isNum = true;
				
				return userInput;
				
			} catch(Exception e) {
				
				print("You did not enter a number :(");
			}
			
		}
	
		return null;
		
	}
	
	
	static String userNumDub(Scanner scanner, String msg) {
		
		boolean isNum = false;
		
		while (isNum == false) {
			
			print (msg + " ");
		
			String userInput = scanner.next();	
			
			try {
				
				Double.parseDouble(userInput);
				
				isNum = true;
				
				return userInput;
				
			} catch(Exception e) {
				
				print("You did not enter a number :(");
			}
			
		}
	
		return null;
		
	}
	
	
	static String userBoo(Scanner scanner, String msg) {
		
		boolean isTrueOrFalse = false;
		
		while (isTrueOrFalse == false) {
			
			print (msg + " ");
		
			String userInput = scanner.next();	
			
			switch(userInput) {
			
			case "yes":
			
			case "no":
			
				isTrueOrFalse = true;
				
				return userInput;
				
			default: 
					System.out.print("Please try again! >:(");
				
			}
			
		}
	
		return null;
		
	}
	
	
	static String userInputBound(Scanner scanner, String msg, int lower, int upper) {
		
		int userNumber = lower - 1;
		
		while (userNumber < lower || userNumber > upper) {
			
			userNumber = Integer.parseInt(userNumInt(scanner, msg));
			
			if (userNumber < lower || userNumber > upper) {
				
				print("Out of bounds!");
			}
			
			else {
				return userNumber + "";
			}
		}
	
		return null;
		
	}
	
	
	
	
	//options
	
	
		
		
	

	
	static Station getStation(Scanner scanner, String msg) {
		
		List<Station> results = new ArrayList<Station>();
		
		do { 
			String stationName = userIn(scanner, msg);
		
			results = new ArrayList<Station>();
			
			for (Station station : stations) {
			
				if (station.getStationName().equalsIgnoreCase(stationName)) {
			
					results.add(station);		
				}
			}
		
			if (results.size() == 0) {
				
				print("Not valid response!");
			}
			
		} while (results.size() == 0);
			
		if (results.size() > 1) { 
			
			printStations(results);
			
			int number = Integer.parseInt (userInputBound(scanner, "Which station number did you mean?", 1, results.size())) - 1;
			
			return results.get(number);
			
		}
		
		return results.get(0);
	}

	
	static void displayNearest(Scanner scanner) {
		
	double	lat = Double.parseDouble(userNumDub(scanner, "Enter your latitude please:"));
		
	double	lng = Double.parseDouble(userNumDub(scanner, "Enter your longitude please:"));
	
	double distance = stationDist(lat, lng, stations.get(0));
	
	Station station = stations.get(0);
	
	
	for (int i = 0; i < stations.size(); i++) {
		
		if(distance > stationDist(lat, lng, stations.get(i))) {
			
		   distance = stationDist(lat, lng, stations.get(i));
			
		   station = stations.get(i);
		   
		}
		 
	}
	
	System.out.println("This is the nearest station to you: \n" + station);
	
	}
	
	
	
	
	
	
	

	private static double stationDist(double lat, double lng, Station station) {
		// TODO Auto-generated method stub
		return 0;
	}

	static void displayByWheelchair(Scanner scanner) {
		
		List<Station> results = new ArrayList<Station>();
		 
		boolean wheelchairAcc = userBoo(scanner, "Weehlchair or no?").equals("yes");
	
		results = new ArrayList<Station>();
		
		for (Station station : stations) {
		
			if (station.getWHEELchair() == wheelchairAcc) {
		
				results.add(station);		
			}
		}
			
		if (results.size() >= 1) { 
			
			printStations(results);	
		}
		
		else if (results.size() == 0) {
			
			print("No!");
			}
		}
		

	static void displayStationNames(Scanner scanner) {
		
		Station station = new Station();
	
		String stationName = userIn(scanner, "What name do you want to name the new station?");
		
		station.setStationName(stationName);
		
		print("New station created " + "\n" + "Name: " + stationName + "\n" + "Lat: 41.999999" + "\n" + "Lng: -87.666666" + "\n" + "Description: elevated" + "\n" + "Wheelchair: no" + "\n" + "-1, 16, -1, 22, 21, 16, 14");
	
		stations.add(station);
	
		}
	

}


